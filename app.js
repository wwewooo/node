const Koa = require('koa');
const app = new Koa();
const mysql = require('mysql')
const router = require('./router/user.js')

let connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '123456',
  database: 'wod'
})

connection.connect()
connection.query('SELECT * FROM userinfo', function (err, res, field) {
  if (err) throw err;
  console.log(res)
})


app.use(async ctx => {
  // ctx.body = 'hello word'
  console.log(ctx.method, ctx.url)
});

// 调用路由中间件
app.use(router.routes()).use(router.allowedMethods())

app.listen(3000);