let Router = require('koa-router')
const router = new Router()


router.get('/', (ctx) => {
        ctx.body = 'hello koa'
    })
    .get('/index', (ctx, next) => {
        ctx.body = "hello index，这是index哦"
    })
// const showMsg = function (status, msg, data) {
//     let obj = {};
//     obj.status = status || 400;
//     obj.msg = msg || '';
//     obj.data = data || null;
//     return obj;
// }

// router.get('/', function (ctx, next) {
//     ctx.body = "all users list ok!";
// });

// router.get('/:id', function (ctx, next) {
//     let id = ctx.params.id;
//     ctx.body = "your id is:" + id + ", thank you !";
//     console.log(222)
// });

// router.post('/post', function (ctx, next) {
//     let id = ctx.request.body.id || 0;
//     ctx.body = "you post data:" + JSON.stringify({
//         id: id
//     });
// });

// router.get("/group/:id", function (ctx, next) {
//     let arr = [{
//             'name': 'zfeig',
//             'age': 25,
//             'sex': 'male',
//             'addr': '广东深圳'
//         },
//         {
//             'name': 'lisi',
//             'age': 28,
//             'sex': 'male',
//             'addr': '四川成都'
//         },
//         {
//             'name': 'chenfeng',
//             'age': 24,
//             'sex': 'female',
//             'addr': '湖北武汉'
//         },
//         {
//             'name': 'zhangyong',
//             'age': 32,
//             'sex': 'male',
//             'addr': '浙江杭州'
//         },
//         {
//             'name': 'zfeig',
//             'age': 22,
//             'sex': 'female',
//             'addr': '广东广州'
//         },
//         {
//             'name': 'zfeig',
//             'age': 24,
//             'sex': 'male',
//             'addr': '湖南长沙'
//         },
//         {
//             'name': 'zfeig',
//             'age': 29,
//             'sex': 'female',
//             'addr': '江苏南京'
//         }
//     ];

//     let id = parseInt(ctx.params.id) || 0;
//     if (id > arr.length - 1) {
//         let info = showMsg(400, 'out of index!', null);
//         console.log(info);
//         ctx.body = info;
//         return;
//     }
//     let data = arr[id];
//     ctx.body = data;
// });

module.exports = router;